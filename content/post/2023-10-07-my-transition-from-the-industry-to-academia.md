---
title: My transition from the industry to academia
date: 2023-10-07
description: |
  Since last year, I switched from working in the private industry to working for a public university. I joined the KOF Swiss Economic Institute, part of the ETH Zürich, as Scientific Software Engineer. My job is to support researchers with everything related to software engineering and architecture.

  My role and the context is very different from what you normally find in the industry. I am also convinced that universities should hire more developers in support roles for their researchers. Let me explain both.
draft: true
---

Since last year, I switched from working in the private industry to working for a public university. I joined the [KOF Swiss Economic Institute](https://kof.ethz.ch/en/), part of the ETH Zürich, as Scientific Software Engineer. My job is to support researchers with everything related to software engineering and architecture.

My role and the context is very different from what you normally find in the industry. I am also convinced that universities should hire more developers in support roles for their researchers. Let me explain both.

### What is so different in academia

There are some striking differences between

###
